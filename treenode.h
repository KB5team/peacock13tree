/* 	treenode.h - Tree node structure description and elementary operation
	Author: Sovkov D.S. ZVBZ-34-17 (KB-5)
*/

typedef struct TreeNode
{
	int Key;
	struct TreeNode *Parent;
	struct TreeNode *LeftSon;
	struct TreeNode *RightSon;    
} TreeNode;

typedef struct LSRB_TreeNode
{
	int Key;
	struct LSRB_TreeNode *Parent;
	struct LSRB_TreeNode *LeftSon;
	struct LSRB_TreeNode *RightBro;
} LSRB_TreeNode;

void Show(TreeNode *Node);
TreeNode *CreateRoot(TreeNode *Root, int Key);
TreeNode *Add(TreeNode *Root, int Key);
TreeNode *Find(TreeNode *Root, int Key);
void Preorder(TreeNode *Root);
void Postorder(TreeNode *Root);
void Inorder(TreeNode *Root);

/* LSRB */

void LSRB_Show(LSRB_TreeNode *Node);
LSRB_TreeNode *LSRB_CreateRoot(LSRB_TreeNode *Root, int Key);
LSRB_TreeNode *LSRB_Add(LSRB_TreeNode *Root, int Key);
LSRB_TreeNode *LSRB_Find(LSRB_TreeNode *Root, int Key);


void AddInorder(TreeNode *Src, TreeNode *Dst);
void AddPrerder(TreeNode *Src, TreeNode *Dst);


