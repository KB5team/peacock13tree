/* 	treenode.h - Tree node structure description and elementary operation
	Author: Sovkov D.S. ZVBZ-34-17 (KB-5)
*/

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "treenode.h"


TreeNode *CreateRoot(TreeNode *Root, int Key)
{
	TreeNode *Tmp = malloc(sizeof(TreeNode));
	Tmp->Key = Key;
	Tmp->Parent = NULL;
	Tmp->LeftSon = Tmp->RightSon = NULL;
	Root = Tmp;
	return Root;
}

LSRB_TreeNode *LSRB_CreateRoot(LSRB_TreeNode *Root, int Key)
{
	LSRB_TreeNode *Tmp = malloc(sizeof(LSRB_TreeNode));
	Tmp->Key = Key;
	Tmp->Parent = NULL;
	Tmp->LeftSon = Tmp->RightBro = NULL;
	Root = Tmp;
	return Root;
}

TreeNode *Add(TreeNode *Root, int Key)
{
	TreeNode *Root2 = Root, *Root3 = NULL;
	TreeNode *Tmp = malloc(sizeof(TreeNode));
	Tmp->Key = Key;

	while(Root2 != NULL)
	{
		Root3 = Root2;
		if(Key < Root2->Key)
			Root2 = Root2->LeftSon;
		else
			Root2 = Root2->RightSon;
	}

	Tmp->Parent = Root3;
	Tmp->LeftSon = NULL;
	Tmp->RightSon = NULL;

	if(Key < Root3->Key) Root3->LeftSon = Tmp;
	else Root3->RightSon = Tmp;
	return Tmp;
};

LSRB_TreeNode *LSRB_Add(LSRB_TreeNode *Root, int Key)
{
	LSRB_TreeNode *TmpNode;

	if(Key < Root->Key)
	{
		if(Root->Key == 0)
		{
			Root->LeftSon = malloc(sizeof(LSRB_TreeNode));
			Root->LeftSon->Key = Key;
			Root->LeftSon->Parent = Root;
			if(Root->Parent != NULL && Root->Parent->Parent != NULL && Root->RightBro == NULL)
			{
				Root->LeftSon->RightBro = Root->Parent;
				Root->LeftSon->RightBro->LeftSon = NULL;
				Root->Parent = Root->Parent->Parent;
				Root->Parent->LeftSon = Root;
				Root->LeftSon->RightBro->Parent = Root;
				Root->RightBro = Root->LeftSon->RightBro->RightBro;
			}
		}
		

		else if (Key > Root->Key)
		{
			// ПРОВЕРИТЬ
			if(Root->Parent == NULL)
			{
				TmpNode = malloc(sizeof(LSRB_TreeNode));
				TmpNode->Key = Key;
				TmpNode->Parent = NULL;
				TmpNode->LeftSon = NULL;
				TmpNode->RightBro = NULL;
				Root->Parent = TmpNode;
				Root = TmpNode;
			}

		}
		
		else
		{
			LSRB_Add(Root->LeftSon,Key);
		}
	

	}
}

TreeNode *Find(TreeNode *Root, int Key)
{
	if((Root==NULL) || (Root->Key=Key))
		return Root;

	if(Key < Root->Key)
		return Find(Root->LeftSon,Key);
	else
		return Find(Root->RightSon,Key);
}

void Preorder(TreeNode *Root)
{
	if(Root==NULL) return;
	if(Root->Key) printf("%d ",Root->Key);
	Preorder(Root->LeftSon);
	Preorder(Root->RightSon);
}


void Postorder(TreeNode *Root)
{
	if(Root==NULL) return;
	Postorder(Root->LeftSon);
	Postorder(Root->RightSon);
	if(Root->Key) printf("%d ",Root->Key);
}

void Inorder(TreeNode *Root)
{
	if(Root==NULL) return;
	Inorder(Root->LeftSon);
	if(Root->Key) printf("%d ",Root->Key);
	Inorder(Root->RightSon);
}

void AddPreorder(TreeNode *Src, TreeNode *Dst)
{
	if(Src==NULL) return;
	if(Src->Key) Add(Dst,Src->Key);
	AddPreorder(Src->LeftSon,Dst);
	AddPreorder(Src->RightSon,Dst);
}

void AddInorder(TreeNode *Src, TreeNode *Dst)
{
	if(Src==NULL) return;
	AddInorder(Src->LeftSon,Dst);
	if(Src->Key) Add(Dst,Src->Key);
	AddInorder(Src->RightSon,Dst);
}

void Show(TreeNode *Node)
{
	printf("-====================-\r\n");
	printf("Node Ptr: \x1b[31;1m %.8X\r\n\x1b[0m",Node);
	printf("Node Key: \x1b[32m %.8d\r\n\x1b[0m",Node->Key);
	printf("Node Left: \x1b[33m %.8X\r\n\x1b[0m",Node->LeftSon);
	printf("Node Right: \x1b[33m %.8X\r\n\x1b[0m",Node->RightSon);
	printf("-====================-\r\n");
}

void LSRB_Show(LSRB_TreeNode *Node)
{
	printf("-======================-\r\n");
	printf("Node Ptr: \x1b[31;1m %.8X\r\n\x1b[0m",Node);
	printf("Node Key: \x1b[32m %.8d\r\n\x1b[0m",Node->Key);
	printf("Node Left son: \x1b[33m %.8X\r\n\x1b[0m",Node->LeftSon);
	printf("Node Right bro: \x1b[33m %.8X\r\n\x1b[0m",Node->RightBro);
	printf("-======================-\r\n");
}

