#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "treenode.h"

/* ZVBZ-34-17 Sovkov D.S. Variant #13 */

int main(void)
{
	TreeNode *TreeA;
	TreeNode *TreeB;

	int TmpA, TmpB;

	printf("Enter values for tree A (0 to stop):\r\n");
	scanf("%d",&TmpA);
	TreeA = CreateRoot(TreeA,TmpA);

	while(TmpA != 0)
	{
		scanf("%d",&TmpA);
		if(TmpA != 0) Add(TreeA,TmpA);
	}

	printf("Enter values for tree B (0 to stop):\r\n");
	scanf("%d",&TmpB);
	TreeB = CreateRoot(TreeB,TmpB);

	while(TmpB != 0)
	{
		scanf("%d",&TmpB);
		if(TmpB != 0) Add(TreeB,TmpB);
	}

	AddInorder(TreeB,TreeA);


    printf("Tree A postorder: ");
    Postorder(TreeA);
    printf("\r\n");
    printf("Tree B inorder: ");
    Inorder(TreeB);



    return 0;

}
